<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240604070529 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE substitutes_exercise (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, code VARCHAR(24) NOT NULL, name VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, raw_text LONGTEXT NOT NULL, chains JSON NOT NULL, INDEX IDX_EA6AF3B4A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE substitutes_exercise ADD CONSTRAINT FK_EA6AF3B4A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exercise CHANGE language language VARCHAR(128) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE substitutes_exercise DROP FOREIGN KEY FK_EA6AF3B4A76ED395');
        $this->addSql('DROP TABLE substitutes_exercise');
        $this->addSql('ALTER TABLE exercise CHANGE language language VARCHAR(128) DEFAULT \'fr\' NOT NULL');
    }
}
