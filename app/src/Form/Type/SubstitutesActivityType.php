<?php

namespace App\Form\Type;

use App\Entity\SubstitutesActivity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubstitutesActivityType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('student')
            ->add('duration')
            ->add('completionRate')
            ->add('errorRate')
            ->add('text')
            ->add('steps', CollectionType::class, [
                'entry_type' => SubstitutesStepType::class,
                'allow_add' => true
            ])
            ->add('actions', CollectionType::class, [
                'entry_type' => ActionType::class,
                'allow_add' => true
            ])
            ->add('date', DateTimeType::class, [
                'widget' => 'single_text'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => SubstitutesActivity::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
