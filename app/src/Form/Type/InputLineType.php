<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class InputLineType extends AbstractType {

    public function buildForm ( FormBuilderInterface $builder, array $options ) {
        $builder
            ->add ( 'timeTaken', NumberType::class )
            ->add ( 'isCorrect', CheckboxType::class )
            ->add ( 'blocks', CollectionType::class, [
                'entry_type' => BlockType::class,
                'allow_add' => true
            ] );
    }
}
