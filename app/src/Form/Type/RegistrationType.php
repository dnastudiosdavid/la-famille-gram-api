<?php

namespace App\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends UserBaseType {

    public function buildForm ( FormBuilderInterface $builder, array $options ) {

        parent::buildForm ( $builder, $options );
    }

    public function getParent () {

        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix () {

        return 'app_user_registration';
    }
}
