<?php

namespace App\Form\Type;

use App\Entity\Activity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActivityType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('student')
            ->add('mode')
            ->add('duration')
            ->add('completionRate')
            ->add('errorRate')
            ->add('steps', CollectionType::class, [
                'entry_type' => StepType::class,
                'allow_add' => true
            ])
            ->add('actions', CollectionType::class, [
                'entry_type' => ActionType::class,
                'allow_add' => true
            ])
            ->add('date', DateTimeType::class, [
                'widget' => 'single_text'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Activity::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }
}
