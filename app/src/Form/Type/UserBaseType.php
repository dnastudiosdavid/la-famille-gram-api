<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UserBaseType extends AbstractType {

    public function buildForm ( FormBuilderInterface $builder, array $options ) {

        $builder->remove ( 'username' );
    }

    public function getBlockPrefix () {

        return 'app_user_base';
    }
}
