<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class WordType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('value')
            ->add('version', TextType::class, [
                'empty_data' => '1'
            ])
            ->add('genders', TextType::class, [
                'empty_data' => ''
            ])
            ->add('numbers', TextType::class, [
                'empty_data' => ''
            ])
            ->add('persons', TextType::class, [
                'empty_data' => ''
            ])
            ->add('cases', TextType::class, [
                'empty_data' => ''
            ]);
    }
}
