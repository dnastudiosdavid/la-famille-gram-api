<?php

namespace App\Form\Type;

use App\Entity\Exercise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ExerciseType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name')
            ->add('language')
            ->add('acceptsLeaderboard')
            ->add('modes', ChoiceType::class, [
                'choices' => [
                    'tirettes' => 'tirettes',
                    'gram-class' => 'gram-class',
                    'silhouettes' => 'silhouettes',
                    'chains' => 'chains'
                ],
                'multiple' => true
            ])
            ->add('sentences', CollectionType::class, [
                "entry_type" => SentenceType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "delete_empty" => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => Exercise::class,
            'csrf_protection' => false
        ]);
    }
}
