<?php

namespace App\Form\Type;

use App\Entity\Exercise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SentenceType extends AbstractType {

    public function buildForm ( FormBuilderInterface $builder, array $options ) {
        $builder
            ->add ( 'name' )
            ->add ( 'degree' )
            ->add ( 'difficulty' )
            ->add ( 'tirettes', CollectionType::class, [
                "entry_type" => TiretteType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "delete_empty" => true,
                'allow_extra_fields' => true
            ] )
            ->add ( 'chains', CollectionType::class, [
                "entry_type" => ChainType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "delete_empty" => true,
                'allow_extra_fields' => true
            ] );
    }
}
