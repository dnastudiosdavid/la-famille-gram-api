<?php

namespace App\Form\Type;

use AppBundle\Document\Student;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class SubstitutesSolutionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        /** Dynamic validation for solutions */
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $submittedData = $event->getData();
            $form = $event->getForm();
            $event->getForm()->setData([]);

            if (is_array($submittedData)) {
                foreach ($submittedData as $key => $value) {
                    $form->add($key, CollectionType::class, [
                        'entry_type' => IntegerType::class,
                        'allow_add' => true,
                        'allow_delete' => true
                    ]);
                }
            }
        });
    }
}
