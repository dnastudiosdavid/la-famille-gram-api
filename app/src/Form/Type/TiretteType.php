<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;

class TiretteType extends AbstractType {

    public function buildForm ( FormBuilderInterface $builder, array $options ) {
        $builder
            ->add ( 'grammaticalClass', GrammaticalClassType::class )
            ->add ( 'chain', ChainType::class, [
                "required" => false
            ] )
            ->add ( 'words', CollectionType::class, [
                "entry_type" => WordType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "delete_empty" => true
            ] );

        $builder->get ( 'chain' )->addModelTransformer ( new CallbackTransformer(
            function ( $originalInput ) {
                return $originalInput;
            },
            function ( $submittedValue ) {
                return $submittedValue["name"] ? $submittedValue : null;
            }
        ) );
    }
}
