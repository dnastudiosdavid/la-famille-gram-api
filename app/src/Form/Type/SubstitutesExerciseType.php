<?php

namespace App\Form\Type;

use App\Entity\SubstitutesExercise;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubstitutesExerciseType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name')
            ->add('title')
            ->add('rawText', TextType::class, [
                "empty_data" => ""
            ])
            ->add('chains', CollectionType::class, [
                "entry_type" => SubstitutesChainType::class,
                "allow_add" => true,
                "allow_delete" => true,
                "delete_empty" => true
            ])
            ->add('solution', SubstitutesSolutionType::class);
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => SubstitutesExercise::class,
            'csrf_protection' => false
        ]);
    }
}
