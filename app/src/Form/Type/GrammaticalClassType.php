<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class GrammaticalClassType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('id')
            ->add('name')
            ->add('isInvariable', CheckboxType::class)
            ->add('requireChain', CheckboxType::class)
            ->add('requireGender', CheckboxType::class)
            ->add('defaultGender', TextType::class, [
                'empty_data' => ''
            ])
            ->add('requireNumber', CheckboxType::class)
            ->add('defaultNumber', TextType::class, [
                'empty_data' => ''
            ])
            ->add('requirePerson', CheckboxType::class)
            ->add('defaultPerson', TextType::class, [
                'empty_data' => ''
            ])
            ->add('requireCase', CheckboxType::class)
            ->add('defaultCase', TextType::class, [
                'empty_data' => ''
            ]);
    }
}
