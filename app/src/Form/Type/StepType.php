<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class StepType extends AbstractType {

    public function buildForm ( FormBuilderInterface $builder, array $options ) {
        $builder
            ->add ( 'instruction' )
            ->add ( 'givenLine', CollectionType::class, [
                'allow_add' => true
            ] )
            ->add ( 'solutionsAmount', NumberType::class )
            ->add ( 'inputLines', CollectionType::class, [
                'entry_type' => InputLineType::class,
                'allow_add' => true
            ] )
            ->add ( 'timeTaken', NumberType::class );
    }
}
