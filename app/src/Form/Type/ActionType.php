<?php

namespace App\Form\Type;

use App\Form\DataTransformer\JsonTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ActionType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('time', TextType::class)
            ->add('scope', TextType::class)
            ->add('action', TextType::class)
            ->add('outcome', TextType::class)
            ->add('data', TextType::class);

        // What the heck starts there
        // convert data into a string to pass the checks
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $submittedData = $event->getData();
            if (isset($submittedData["data"])) {
                $submittedData["data"] = json_encode($submittedData["data"]);
            }
            $event->setData($submittedData);
        });

        // convert data back to json
        $builder->get('data')
            ->addModelTransformer(new JsonTransformer());
    }
}
