<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;

class SubstitutesStepType extends AbstractType {

    public function buildForm ( FormBuilderInterface $builder, array $options ) {
        $builder
            ->add ( 'time', NumberType::class )
            ->add ( 'groups', CollectionType::class, [
                'entry_type' => SubstitutesGroupType::class,
                'allow_add' => true
            ] );
    }
}
