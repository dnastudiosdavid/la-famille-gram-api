<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SubstitutesGroupType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', TextType::class)
            ->add('color', TextType::class)
            ->add('correct', CollectionType::class, [
                'entry_type' => NumberType::class,
                'allow_add' => true
            ])
            ->add('wrong', CollectionType::class, [
                'entry_type' => NumberType::class,
                'allow_add' => true
            ])
            ->add('missing', CollectionType::class, [
                'entry_type' => NumberType::class,
                'allow_add' => true
            ]);
    }
}
