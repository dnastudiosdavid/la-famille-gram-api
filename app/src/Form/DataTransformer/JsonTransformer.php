<?php

namespace App\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class JsonTransformer implements DataTransformerInterface {
    public function transform($value) {
        return $value ? json_encode($value) : '';
    }

    public function reverseTransform($value) {
        return $value ? json_decode($value, true) : [];
    }
}
