<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * TODO: should be renamed to SentenceExercise but that would probably require a rename of the db table as well
 * @ORM\Entity(repositoryClass="App\Repository\ExerciseRepository")
 */
class Exercise implements ExerciseInterface {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"headers", "details"})
 */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="exercises")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Exclude()
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"headers", "details"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $language;

    /**
     * @ORM\Column(type="string", length=24)
     * @Serializer\Groups({"headers", "details"})
     */
    private $code;

    /**
     * @ORM\Column(type="boolean")
     */
    private $acceptsLeaderboard;

    /**
     * @ORM\Column(type="array")
     */
    private $modes = [];

    /**
     * @ORM\Column(type="json")
     */
    private $sentences = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Score", mappedBy="exercise", cascade={"remove"})
     * @Serializer\Exclude()
     */
    private $scores;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Activity", mappedBy="exercise", orphanRemoval=true, cascade={"remove"})
     * @Serializer\Exclude()
     */
    private $activities;

    public function __construct() {
        $this->scores = new ArrayCollection();
        $this->activities = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getData(): ?array {
        return $this->data;
    }

    public function setData(array $data): self {
        $this->data = $data;

        return $this;
    }

    public function getCode(): ?string {
        return $this->code;
    }

    public function setCode(string $code): self {
        $this->code = $code;
        return $this;
    }

    public function getLanguage(): ?string {
        return $this->language;
    }

    public function setLanguage(string $code): self {
        $this->language = $code;
        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;

        return $this;
    }

    public function getAcceptsLeaderboard(): ?bool {
        return $this->acceptsLeaderboard;
    }

    public function setAcceptsLeaderboard(bool $acceptsLeaderboard): self {
        $this->acceptsLeaderboard = $acceptsLeaderboard;

        return $this;
    }

    public function getModes(): ?array {
        return $this->modes;
    }

    public function setModes(array $modes): self {
        $this->modes = $modes;

        return $this;
    }

    public function getSentences(): ?array {
        return $this->sentences;
    }

    public function setSentences(array $sentences): self {
        $this->sentences = $sentences;

        return $this;
    }

    /**
     * @return Collection|Score[]
     */
    public function getScores(): Collection {
        return $this->scores;
    }

    /**
     * @param Score $score
     * @return $this
     */
    public function addScore(Score $score): self {
        if (!$this->scores->contains($score)) {
            $this->scores[] = $score;
            $score->setExercise($this);
        }

        return $this;
    }

    /**
     * @param Score $score
     * @return $this
     */
    public function removeScore(Score $score): self {
        if ($this->scores->contains($score)) {
            $this->scores->removeElement($score);
            // set the owning side to null (unless already changed)
            if ($score->getExercise() === $this) {
                $score->setExercise(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Activity[]
     */
    public function getActivities(): Collection {
        return $this->activities;
    }

    /**
     * @param Activity $activity
     * @return $this
     */
    public function addActivity(Activity $activity): self {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
            $activity->setExercise($this);
        }

        return $this;
    }

    /**
     * @param Activity $activity
     * @return $this
     */
    public function removeActivity(Activity $activity): self {
        if ($this->activities->contains($activity)) {
            $this->activities->removeElement($activity);
            // set the owning side to null (unless already changed)
            if ($activity->getExercise() === $this) {
                $activity->setExercise(null);
            }
        }

        return $this;
    }
}
