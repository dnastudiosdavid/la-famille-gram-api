<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubstitutesActivityRepository")
 */
class SubstitutesActivity {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"headers", "details"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=16)
     * @Serializer\Groups({"headers", "details"})
     */
    private $student;

    /**
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"headers", "details"})
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SubstitutesExercise", inversedBy="activities")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Serializer\Groups({"headers", "details"})
     */
    private $exercise;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"headers", "details"})
     */
    private $duration;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"headers", "details"})
     */
    private $completionRate;

    /**
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"headers", "details"})
     */
    private $errorRate;

    /**
     * @ORM\Column(type="text")
     * @Serializer\Groups({"details"})
     */
    private $text = "";

    /**
     * @ORM\Column(type="json")
     * @Serializer\Groups({"details"})
     */
    private $steps = [];

    /**
     * @ORM\Column(type="json")
     * @Serializer\Groups({"details"})
     */
    private $actions = [];

    public function getId(): ?int {
        return $this->id;
    }

    public function getStudent(): ?string {
        return $this->student;
    }

    public function setStudent(string $student): self {
        $this->student = $student;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface {
        return $this->date;
    }

    public function setDate(DateTime $date): self {
        $this->date = $date;

        return $this;
    }

    public function getExercise(): ?SubstitutesExercise {
        return $this->exercise;
    }

    public function setExercise(?SubstitutesExercise $exercise): self {
        $this->exercise = $exercise;

        return $this;
    }

    public function getDuration(): ?int {
        return $this->duration;
    }

    public function setDuration(int $duration): self {
        $this->duration = $duration;

        return $this;
    }

    public function getCompletionRate(): ?int {
        return $this->completionRate;
    }

    public function setCompletionRate(int $completionRate): self {
        $this->completionRate = $completionRate;

        return $this;
    }

    public function getErrorRate(): ?int {
        return $this->errorRate;
    }

    public function setErrorRate(int $errorRate): self {
        $this->errorRate = $errorRate;

        return $this;
    }

    public function getSteps(): ?array {
        return $this->steps;
    }

    public function setSteps(array $steps): self {
        $this->steps = $steps;

        return $this;
    }

    public function setText(string $text): SubstitutesActivity {
        $this->text = $text;
        return $this;
    }

    public function getText(): string {
        return $this->text;
    }

    public function setActions(array $actions): SubstitutesActivity {
        $this->actions = $actions;
        return $this;
    }

    public function getActions(): array {
        return $this->actions;
    }
}
