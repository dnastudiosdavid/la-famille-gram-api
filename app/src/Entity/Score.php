<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ScoreRepository")
 */
class Score {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $nickname;

    /**
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $mode;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Exercise", inversedBy="scores")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */
    private $exercise;

    public function getId (): ?int {
        return $this->id;
    }

    public function getNickname (): ?string {
        return $this->nickname;
    }

    public function setNickname ( string $nickname ): self {
        $this->nickname = $nickname;

        return $this;
    }

    public function getScore (): ?int {
        return $this->score;
    }

    public function setScore ( int $score ): self {
        $this->score = $score;

        return $this;
    }

    public function getMode (): ?string {
        return $this->mode;
    }

    public function setMode ( string $mode ): self {
        $this->mode = $mode;

        return $this;
    }

    public function getExercise (): ?Exercise {
        return $this->exercise;
    }

    public function setExercise ( ?Exercise $exercise ): self {
        $this->exercise = $exercise;

        return $this;
    }
}
