<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubstitutesRepository")
 */
class SubstitutesExercise implements ExerciseInterface {

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"headers", "details"})
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="exercises")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Exclude()
     */
    private User $user;

    /**
     * @ORM\Column(type="string", length=24)
     * @Serializer\Groups({"headers", "details"})
     */
    private string $code;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"headers", "details"})
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $title;

    /**
     * @ORM\Column(type="text")
     */
    private string $rawText;

    /**
     * @ORM\Column(type="json")
     */
    private array $chains;

    /**
     * @ORM\Column(type="json")
     */
    private array $solution;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SubstitutesActivity", mappedBy="exercise", orphanRemoval=true, cascade={"remove"})
     * @Serializer\Exclude()
     */
    private $activities;

    public function __construct() {
        $this->activities = new ArrayCollection();
    }

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;
        return $this;
    }

    public function getCode(): ?string {
        return $this->code;
    }

    public function setCode(string $code): self {
        $this->code = $code;
        return $this;
    }

    public function getUser(): ?User {
        return $this->user;
    }

    public function setUser(?User $user): self {
        $this->user = $user;
        return $this;
    }

    public function getTitle(): string {
        return $this->title;
    }

    public function setTitle(string $title): void {
        $this->title = $title;
    }

    public function getRawText(): string {
        return $this->rawText;
    }

    public function setRawText(string $rawText): void {
        $this->rawText = $rawText;
    }

    public function getChains(): array {
        return $this->chains;
    }

    public function setChains(array $chains): void {
        $this->chains = $chains;
    }

    public function getSolution(): array {
        return $this->solution;
    }

    public function setSolution(array $solution): void {
        $this->solution = $solution;
    }

    /**
     * @return Collection|SubstitutesActivity []
     */
    public function getActivities(): Collection {
        return $this->activities;
    }

    /**
     * @param SubstitutesActivity $activity
     * @return $this
     */
    public function addActivity(SubstitutesActivity $activity): self {
        if (!$this->activities->contains($activity)) {
            $this->activities[] = $activity;
            $activity->setExercise($this);
        }

        return $this;
    }

    /**
     * @param SubstitutesActivity $activity
     * @return $this
     */
    public function removeActivity(SubstitutesActivity $activity): self {
        if ($this->activities->contains($activity)) {
            $this->activities->removeElement($activity);
            // set the owning side to null (unless already changed)
            if ($activity->getExercise() === $this) {
                $activity->setExercise(null);
            }
        }

        return $this;
    }
}
