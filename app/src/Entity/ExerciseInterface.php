<?php

namespace App\Entity;

interface ExerciseInterface {

    public function getUser(): ?User;
}
