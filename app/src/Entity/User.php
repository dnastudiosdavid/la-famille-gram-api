<?php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @Serializer\ExclusionPolicy("all")
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Email setter override.
     *
     * @param string $email
     *
     * @return $this
     */
    public function setEmail ( $email ) {

        parent::setUsername ( $email );
        return parent::setEmail ( $email );
    }

    /**
     * Email canonical setter override.
     *
     * @param string $emailCanonical
     *
     * @return $this
     */
    public function setEmailCanonical ( $emailCanonical ) {

        parent::setUsernameCanonical ( $emailCanonical );
        return parent::setEmailCanonical ( $emailCanonical );
    }

    public function __construct () {
        parent::__construct ();
    }
}
