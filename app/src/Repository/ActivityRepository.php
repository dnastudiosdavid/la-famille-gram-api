<?php

namespace App\Repository;

use App\Entity\Activity;
use App\Entity\Exercise;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Activity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Activity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Activity[]    findAll()
 * @method Activity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ActivityRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Activity::class);
    }

    /**
     * Gets all the activities of the given exercise.
     *
     * @param Exercise $exercise
     * @param $start
     * @param $limit
     * @return array
     */
    function getActivitiesOfExercise(Exercise $exercise, $start, $limit): array {
        return $this->createQueryBuilder('a')
            ->select('a.id, a.student, a.mode, a.date, a.duration, a.completionRate, a.errorRate')
            ->where('a.exercise = :exercise')
            ->orderBy('a.date', 'DESC')
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->setParameter('exercise', $exercise)
            ->getQuery()
            ->getResult();
    }

    /**
     * Gets all the activities of the given user.
     *
     * @param User $user
     * @param $limit
     *
     * @return array
     */
    function getActivitiesOfUser(User $user, $limit): array {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->join('a.exercise', 'e')
            ->where('e.user = :user')
            ->orderBy('a.date', 'DESC')
            ->setMaxResults($limit)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * Gets the amount of activities.
     *
     * @return int
     *
     * @throws NonUniqueResultException
     */
    public function getActivitiesCount(): int {
        return $this->createQueryBuilder('a')
            ->select("COUNT(a.id)")
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Gets the activities count grouped by modes.
     *
     * @return array
     */
    public function getActivitiesCountByMode(): array {
        return $this->createQueryBuilder('a')
            ->select("a.mode, COUNT(a.id) as count")
            ->groupBy("a.mode")
            ->getQuery()
            ->getArrayResult();
    }
}
