<?php

namespace App\Repository;

use App\Entity\SubstitutesActivity;
use App\Entity\SubstitutesExercise;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubstitutesActivity|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubstitutesActivity|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubstitutesActivity[]    findAll()
 * @method SubstitutesActivity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubstitutesActivityRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, SubstitutesActivity::class);
    }

    /**
     * Gets all the activities of the given exercise.
     *
     * @param SubstitutesExercise $exercise
     * @param $start
     * @param $limit
     *
     * @return array
     */
    function getActivitiesOfExercise(SubstitutesExercise $exercise, $start, $limit): array {
        return $this->createQueryBuilder('a')
            ->select('a.id, a.student, a.date, a.completionRate, a.errorRate, a.duration')
            ->where('a.exercise = :exercise')
            ->orderBy('a.date', 'DESC')
            ->setParameter('exercise', $exercise)
            ->setFirstResult($start)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * Gets all the activities of the given user.
     *
     * @param User $user
     * @param                  $limit
     *
     * @return array
     */
    function getActivitiesOfUser(User $user, $limit): array {
        return $this->createQueryBuilder('a')
            ->select('a')
            ->join('a.exercise', 'e')
            ->where('e.user = :user')
            ->orderBy('a.date', 'DESC')
            ->setMaxResults($limit)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * Gets the amount of activities.
     *
     * @return int
     *
     * @throws NonUniqueResultException|NoResultException
     */
    public function getActivitiesCount(): int {
        return $this->createQueryBuilder('a')
            ->select("COUNT(a.id)")
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Gets the activities count grouped by modes.
     *
     * @return array
     */
    public function getActivitiesCountByMode(): array {
        return $this->createQueryBuilder('a')
            ->select("a.mode, COUNT(a.id) as count")
            ->groupBy("a.mode")
            ->getQuery()
            ->getArrayResult();
    }
}
