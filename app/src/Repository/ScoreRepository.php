<?php

namespace App\Repository;

use App\Entity\Exercise;
use App\Entity\Score;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Score|null find( $id, $lockMode = null, $lockVersion = null )
 * @method Score|null findOneBy( array $criteria, array $orderBy = null )
 * @method Score[]    findAll()
 * @method Score[]    findBy( array $criteria, array $orderBy = null, $limit = null, $offset = null )
 */
class ScoreRepository extends ServiceEntityRepository {

    public function __construct ( ManagerRegistry $registry ) {
        parent::__construct ( $registry, Score::class );
    }

    /**
     * Gets all the scores of an exercise given its code.
     *
     * @param string $exerciseCode
     * @param string $mode
     *
     * @return array
     */
    function getScoresOf ( string $exerciseCode, string $mode ): array {
        return $this->createQueryBuilder ( 's' )
            ->select ( 's.id, s.nickname, s.score, s.mode' )
            ->join ( 's.exercise', 'e' )
            ->where ( 'e.code LIKE :code' )
            ->andWhere ( 's.mode LIKE :mode' )
            ->orderBy ( 's.score', 'DESC' )
            ->addOrderBy ( 's.id', 'ASC' )
            ->setParameter ( 'code', $exerciseCode )
            ->setParameter ( 'mode', $mode )
            ->getQuery ()
            ->getResult ();
    }
}
