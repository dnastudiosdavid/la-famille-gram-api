<?php

namespace App\Repository;

use App\Entity\Exercise;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Exercise|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exercise|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exercise[]    findAll()
 * @method Exercise[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExerciseRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, Exercise::class);
    }

    /**
     * Gets all the exercises of the given user.
     *
     * @param User $user
     *
     * @return array
     */
    public function getExercisesOf(User $user): array {
        return $this->createQueryBuilder('e')
            ->select('e.name, e.code, e.id, e.language')
            ->where('e.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * Gets an exercise by its code.
     *
     * @param string $code
     *
     * @return Exercise
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getExerciseByCode(string $code): Exercise {
        $query = $this->createQueryBuilder('e')
            ->select('partial e.{id, name, code, acceptsLeaderboard, modes, sentences, language}')
            ->where('e.code LIKE :code')
            ->setParameter('code', $code)
            ->getQuery();

        $query->setHint(Query::HINT_FORCE_PARTIAL_LOAD, 1);

        return $query->getSingleResult();
    }

    /**
     * Gets an exercise given its id.
     *
     * @param int $id
     *
     * @return Exercise
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getExercise(int $id): Exercise {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.id LIKE :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Gets the amount of exercises.
     *
     * @return int
     * @throws NonUniqueResultException
     */
    function getExercisesCount(): int {

        return $this->createQueryBuilder('e')
            ->select('COUNT(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
