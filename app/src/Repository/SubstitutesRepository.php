<?php

namespace App\Repository;

use App\Entity\SubstitutesExercise;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SubstitutesExercise|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubstitutesExercise|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubstitutesExercise[]    findAll()
 * @method SubstitutesExercise[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubstitutesRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, SubstitutesExercise::class);
    }

    /**
     * Gets all the exercises of the given user.
     *
     * @param User $user
     *
     * @return array
     */
    public function getExercisesOf(User $user): array {
        return $this->createQueryBuilder('e')
            ->select('e.id, e.name, e.code')
            ->where('e.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * Gets an exercise given its id.
     *
     * @param int $id
     *
     * @return SubstitutesExercise
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getExercise(int $id): SubstitutesExercise {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.id LIKE :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Gets an exercise by its code.
     *
     * @param string $code
     *
     * @return SubstitutesExercise
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function getExerciseByCode(string $code): SubstitutesExercise {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.code LIKE :code')
            ->setParameter('code', $code)
            ->getQuery()
            ->getSingleResult();
    }
}
