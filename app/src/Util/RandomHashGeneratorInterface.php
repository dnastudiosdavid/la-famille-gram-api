<?php

namespace App\Util;

interface RandomHashGeneratorInterface {

    /**
     * Gets a random string.
     *
     * @param int $length bytes length
     *
     * @return string
     */
    function getRandomString ( int $length ): string;
}
