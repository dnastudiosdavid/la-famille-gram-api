<?php

namespace App\Util;

class RandomHashGenerator implements RandomHashGeneratorInterface {

    /**
     * Gets a random string.
     *
     * @param int $length
     *
     * @return string
     * @throws \Exception
     */
    function getRandomString ( int $length ): string {
        return bin2hex ( random_bytes ( $length ) );
    }
}
