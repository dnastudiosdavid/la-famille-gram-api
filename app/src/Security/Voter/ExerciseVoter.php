<?php

namespace App\Security\Voter;

use App\Entity\Exercise;
use App\Entity\ExerciseInterface;
use App\Entity\User;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ExerciseVoter extends Voter {

    const CREATE = "create";
    const EDIT = "edit";
    const VIEW = "view";
    const DELETE = "delete";

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject) {
        if (!in_array($attribute, [
            self::CREATE,
            self::EDIT,
            self::VIEW,
            self::DELETE
        ])) {
            return false;
        }

        if (!$subject instanceof ExerciseInterface) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token) {

        $user = $token->getUser();

        // API should already be secured by the firewall
        if (!$user instanceof User) {
            throw new LogicException("This endpoint is not correctly secured.");
        }

        /** @var Exercise $exercise */
        $exercise = $subject;

        switch ($attribute) {
            case self::CREATE:
                return $this->canCreate($user, $exercise);
            case self::VIEW:
                return $this->canView($user, $exercise);
            case self::EDIT:
                return $this->canEdit($user, $exercise);
            case self::DELETE:
                return $this->canDelete($user, $exercise);
        }

        throw new LogicException("You fell into the void!");
    }

    /**
     * Determines if the user can create an exercise.
     *
     * @param User $user
     *
     * @return bool true if the user has the right to do it.
     */
    private function canCreate(User $user): bool {
        // by default, all users can create a new exercise.
        return true;
    }

    /**
     * Determines if the user can view an exercise.
     *
     * @param User $user
     * @param ExerciseInterface $exercise
     *
     * @return bool true if the user has the right to do it
     */
    private function canView(User $user, ExerciseInterface $exercise): bool {
        // only the owner can view his exercise
        return $exercise->getUser() === $user;
    }

    /**
     * Determines if the user can edit an exercise.
     *
     * @param User $user
     * @param ExerciseInterface $exercise
     *
     * @return bool true if the user has the right to do it
     */
    private function canEdit(User $user, ExerciseInterface $exercise): bool {
        // only the owner can edit his exercise
        return $exercise->getUser() === $user;
    }

    /**
     * Determines if the user can delete an exercise.
     *
     * @param User $user
     * @param ExerciseInterface $exercise
     *
     * @return bool true if the user has the right to do it
     */
    private function canDelete(User $user, ExerciseInterface $exercise): bool {
        // only the owner can delete his exercise
        return $exercise->getUser() === $user;
    }
}
