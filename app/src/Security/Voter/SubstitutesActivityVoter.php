<?php

namespace App\Security\Voter;

use App\Entity\SubstitutesActivity;
use App\Entity\User;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SubstitutesActivityVoter extends Voter {

    const VIEW = "view";

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject) {
        if (!in_array($attribute, [
            self::VIEW
        ])) {
            return false;
        }

        if (!$subject instanceof SubstitutesActivity) {
            return false;
        }

        return true;
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token) {

        $user = $token->getUser();

        // API should already be secured by the firewall
        if (!$user instanceof User) {
            throw new LogicException("This endpoint is not correctly secured.");
        }

        /** @var SubstitutesActivity $activity */
        $activity = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($user, $activity);
        }

        throw new LogicException("You fell into the void!");
    }

    /**
     * Determines if the user can view an activity.
     *
     * @param User $user
     * @param SubstitutesActivity $activity
     *
     * @return bool true if the user has the right to do it
     */
    private function canView(User $user, SubstitutesActivity $activity): bool {
        // only the owner can view his exercise
        return $activity->getExercise()->getUser() === $user;
    }
}
