<?php

namespace App\Exercise;

use App\Entity\SubstitutesActivity;
use App\Entity\SubstitutesExercise;
use App\Entity\User;
use App\Repository\SubstitutesActivityRepository;
use App\Repository\SubstitutesRepository;
use App\Util\RandomHashGeneratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * This class manages any exercise manipulation.
 */
class SubstitutesService implements SubstitutesServiceInterface {

    /**
     * Format of the stored code.
     */
    const CODE_FORMAT = "%s_%s";

    private EntityManagerInterface $em;

    private RandomHashGeneratorInterface $randomHashGenerator;

    /**
     * Ctor.
     *
     * @param EntityManagerInterface $em
     * @param RandomHashGeneratorInterface $randomHashGenerator
     */
    public function __construct(EntityManagerInterface $em, RandomHashGeneratorInterface $randomHashGenerator) {
        $this->em = $em;
        $this->randomHashGenerator = $randomHashGenerator;
    }

    /**
     * Persists for the first time the given exercise.
     *
     * @param SubstitutesExercise $exercise
     * @param User $user
     * @return SubstitutesExercise
     */
    function persistExercise(SubstitutesExercise $exercise, User $user): SubstitutesExercise {
        $exercise->setUser($user);
        $exercise->setCode("TMP");
        $this->em->persist($exercise);
        $this->em->flush();
        $exercise->setCode(sprintf(self::CODE_FORMAT, $exercise->getId(), $this->randomHashGenerator->getRandomString(2)));
        $this->em->flush();
        return $exercise;
    }

    /**
     * Updates the given exercise.
     *
     * @param SubstitutesExercise $exercise
     *
     * @return SubstitutesExercise
     */
    function updateExercise(SubstitutesExercise $exercise): SubstitutesExercise {
        $this->em->flush();
        return $exercise;
    }

    /**
     * Gets all the exercises of the given user.
     *
     * @param User $user
     *
     * @return array
     */
    function getExercisesOf(User $user): array {
        /** @var SubstitutesRepository $rep */
        $rep = $this->em->getRepository(SubstitutesExercise::class);
        return $rep->getExercisesOf($user);
    }

    /**
     * Gets an exercise given its id.
     *
     * @param int $id
     * @return SubstitutesExercise|null
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    function getExercise(int $id): ?SubstitutesExercise {
        /** @var SubstitutesRepository $rep */
        $rep = $this->em->getRepository(SubstitutesExercise::class);
        return $rep->getExercise($id);
    }

    /**
     * Deletes the given exercise.
     *
     * @param SubstitutesExercise $exercise
     */
    function deleteExercise(SubstitutesExercise $exercise): void {
        $this->em->remove($exercise);
        $this->em->flush();
    }

    /**
     * Gets an exercise using its code.
     *
     * @param string $code
     *
     * @return SubstitutesExercise
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    function getExercisesByCode(string $code): SubstitutesExercise {
        /** @var SubstitutesRepository $rep */
        $rep = $this->em->getRepository(SubstitutesExercise::class);
        return $rep->getExerciseByCode($code);
    }

    /**
     * @inheritDoc
     *
     * @param SubstitutesActivity $activity
     * @param string $code
     * @return void
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    function persistActivity(SubstitutesActivity $activity, string $code): SubstitutesActivity {
        /** @var SubstitutesRepository $rep */
        $rep = $this->em->getRepository(SubstitutesExercise::class);
        $exercise = $rep->getExerciseByCode($code);
        $activity->setExercise($exercise);
        $this->em->persist($activity);
        $this->em->flush();
        return $activity;
    }

    /**
     * @inheritDoc
     * @param SubstitutesExercise $exercise
     * @return array
     */
    function getActivitiesOfExercise(SubstitutesExercise $exercise, $start, $limit): array {
        /** @var SubstitutesActivityRepository $rep */
        $rep = $this->em->getRepository(SubstitutesActivity::class);
        return $rep->getActivitiesOfExercise($exercise, $start, $limit);
    }

    /**
     * @inheritDoc
     * @param User $user
     * @param $limit
     * @return array
     */
    function getActivitiesOfUser(User $user, $limit): array {
        /** @var SubstitutesActivityRepository $rep */
        $rep = $this->em->getRepository(SubstitutesActivity::class);
        return $rep->getActivitiesOfUser($user, $limit);
    }
}
