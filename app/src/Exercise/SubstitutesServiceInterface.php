<?php

namespace App\Exercise;

use App\Entity\SubstitutesActivity;
use App\Entity\SubstitutesExercise;
use App\Entity\User;

interface SubstitutesServiceInterface {

    /**
     * Persists for the first time the given exercise.
     *
     * @param SubstitutesExercise $exercise
     * @param User $user
     *
     * @return SubstitutesExercise
     */
    function persistExercise(SubstitutesExercise $exercise, User $user): SubstitutesExercise;

    /**
     * Updates the given exercise.
     *
     * @param SubstitutesExercise $exercise
     *
     * @return SubstitutesExercise
     */
    function updateExercise(SubstitutesExercise $exercise): SubstitutesExercise;

    /**
     * Gets all the exercises of the given user.
     *
     * @param User $user
     *
     * @return array
     */
    function getExercisesOf(User $user): array;

    /**
     * Gets an exercise given its id.
     *
     * @param int $id
     */
    function getExercise(int $id): ?SubstitutesExercise;

    /**
     * Deletes the given exercise.
     *
     * @param SubstitutesExercise $exercise
     */
    function deleteExercise(SubstitutesExercise $exercise): void;

    /**
     * Gets an exercise using his code.
     *
     * @param string $code
     *
     * @return SubstitutesExercise|null
     */
    function getExercisesByCode(string $code): SubstitutesExercise;

    /**
     * Persists a new activity.
     *
     * @param SubstitutesActivity $activity
     * @param string $code
     * @return SubstitutesActivity
     */
    function persistActivity(SubstitutesActivity $activity, string $code): SubstitutesActivity;

    /**
     * Gets all the activities of the given exercise.
     *
     * @param SubstitutesExercise $exercise
     * @param $start
     * @param $limit
     *
     * @return array
     */
    function getActivitiesOfExercise(SubstitutesExercise $exercise, $start, $limit): array;

    /**
     * Gets all the activities of the given user.
     *
     * @param User $user
     * @param       $limit
     *
     * @return array
     */
    function getActivitiesOfUser(User $user, $limit): array;
}
