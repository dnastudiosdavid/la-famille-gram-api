<?php

namespace App\Exercise;

use App\Entity\Activity;
use App\Entity\Exercise;
use App\Entity\Score;
use App\Entity\User;

interface SentencesServiceInterface {

    /**
     * Persists for the first time the given exercise.
     *
     * @param Exercise $exercise
     * @param User $user
     *
     * @return Exercise
     */
    function persistExercise(Exercise $exercise, User $user): Exercise;

    /**
     * Updates the given exercise.
     *
     * @param Exercise $exercise
     *
     * @return Exercise
     */
    function updateExercise(Exercise $exercise): Exercise;

    /**
     * Gets all the exercises of the given user.
     *
     * @param User $user
     *
     * @return array
     */
    function getExercisesOf(User $user): array;

    /**
     * Deletes the given exercise.
     *
     * @param Exercise $exercise
     */
    function deleteExercise(Exercise $exercise): void;

    /**
     * Gets an exercise using his code.
     *
     * @param string $code
     *
     * @return mixed
     */
    function getExercisesByCode(string $code);

    /**
     * Persists for the first time the given score.
     *
     * @param Score $score
     * @param string $exerciseCode
     *
     * @return Score
     */
    function persistScore(Score $score, string $exerciseCode): Score;

    /**
     * Gets the leaderboard for the given exercise code.
     *
     * @param string $exerciseCode
     * @param string $mode
     *
     * @return array
     */
    function getLeaderboardOf(string $exerciseCode, string $mode): array;

    /**
     * Persists for the first time the given activity.
     *
     * @param Activity $activity
     * @param string $exerciseCode
     *
     * @return Activity
     */
    function persistActivity(Activity $activity, string $exerciseCode): Activity;

    /**
     * Gets all the activities of the given exercise.
     *
     * @param Exercise $exercise
     * @param $start
     * @param $limit
     * @return array
     */
    function getActivitiesOfExercise(Exercise $exercise, $start, $limit): array;

    /**
     * Gets all the activities of the given user.
     *
     * @param User $user
     * @param        $limit
     *
     * @return array
     */
    function getActivitiesOfUser(User $user, $limit): array;

    /**
     * Gets an exercise given its id.
     *
     * @param int $id
     *
     * @return Exercise|null
     */
    function getExercise(int $id): Exercise;

    /**
     * Gets the amount of activities.
     *
     * @return int
     */
    function getActivitiesCount(): int;

    /**
     * Gets the amount of exercices.
     *
     * @return int
     */
    function getExercisesCount(): int;

    /**
     * Gets the activities count grouped by modes.
     *
     * @return array
     */
    function getActivitiesCountByMode(): array;


    /**
     * Updates the given score.
     *
     * @param Score $score
     *
     * @return mixed
     */
    function updateScore(Score $score): Score;
}
