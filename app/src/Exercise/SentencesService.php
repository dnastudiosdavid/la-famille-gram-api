<?php

namespace App\Exercise;

use App\Entity\Activity;
use App\Entity\Exercise;
use App\Entity\Score;
use App\Entity\User;
use App\Repository\ActivityRepository;
use App\Repository\ExerciseRepository;
use App\Repository\ScoreRepository;
use App\Util\RandomHashGeneratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * This class manages any exercise manipulation.
 */
class SentencesService implements SentencesServiceInterface {

    /**
     * Format of the stored code.
     */
    const CODE_FORMAT = "%s_%s";

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var RandomHashGeneratorInterface
     */
    private $randomHashGenerator;

    /**
     * Ctor.
     *
     * @param EntityManagerInterface $em
     * @param RandomHashGeneratorInterface $randomHashGenerator
     */
    public function __construct(EntityManagerInterface $em, RandomHashGeneratorInterface $randomHashGenerator) {
        $this->em = $em;
        $this->randomHashGenerator = $randomHashGenerator;
    }

    /**
     * Persists for the first time the given exercise.
     *
     * @param Exercise $exercise
     * @param User $user
     *
     * @return Exercise
     */
    function persistExercise(Exercise $exercise, User $user): Exercise {
        $exercise->setUser($user);
        $exercise->setCode("TMP");
        $this->em->persist($exercise);
        $this->em->flush();
        $exercise->setCode(sprintf(self::CODE_FORMAT, $exercise->getId(), $this->randomHashGenerator->getRandomString(2)));
        $this->em->flush();
        return $exercise;
    }

    /**
     * Updates the given exercise.
     *
     * @param Exercise $exercise
     *
     * @return Exercise
     */
    function updateExercise(Exercise $exercise): Exercise {
        $this->em->flush();
        return $exercise;
    }

    /**
     * Gets all the exercises of the given user.
     *
     * @param User $user
     *
     * @return array
     */
    function getExercisesOf(User $user): array {
        /** @var ExerciseRepository $rep */
        $rep = $this->em->getRepository(Exercise::class);
        return $rep->getExercisesOf($user);
    }

    /**
     * Deletes the given exercise.
     *
     * @param Exercise $exercise
     */
    function deleteExercise(Exercise $exercise): void {
        $this->em->remove($exercise);
        $this->em->flush();
    }

    /**
     * Gets an exercise using its code.
     *
     * @param string $code
     *
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    function getExercisesByCode(string $code) {
        /** @var ExerciseRepository $rep */
        $rep = $this->em->getRepository(Exercise::class);
        return $rep->getExerciseByCode($code);
    }

    /**
     * Persists for the first time the given score.
     *
     * @param Score $score
     * @param string $exerciseCode
     *
     * @return Score
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    function persistScore(Score $score, string $exerciseCode): Score {
        /** @var ExerciseRepository $rep */
        $rep = $this->em->getRepository(Exercise::class);
        $exercise = $rep->getExerciseByCode($exerciseCode);
        $score->setExercise($exercise);
        $this->em->persist($score);
        $this->em->flush();
        return $score;
    }

    /**
     * Updates a score.
     *
     * @param Score $score
     *
     * @return Score
     */
    function updateScore(Score $score): Score {
        $this->em->flush();
        return $score;
    }

    /**
     * Gets the leaderboard for the given exercise code.
     *
     * @param string $exerciseCode
     * @param string $mode
     *
     * @return array
     */
    function getLeaderboardOf(string $exerciseCode, string $mode): array {
        /** @var ScoreRepository $rep */
        $rep = $this->em->getRepository(Score::class);
        return $rep->getScoresOf($exerciseCode, $mode);
    }

    /**
     * Persists for the first time the given activity.
     *
     * @param Activity $activity
     * @param string $exerciseCode
     *
     * @return mixed
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    function persistActivity(Activity $activity, string $exerciseCode): Activity {
        /** @var ExerciseRepository $rep */
        $rep = $this->em->getRepository(Exercise::class);
        $exercise = $rep->getExerciseByCode($exerciseCode);
        $activity->setExercise($exercise);
        $this->em->persist($activity);
        $this->em->flush();
        return $activity;
    }

    /**
     * Gets all the activities of the given exercise.
     *
     * @param Exercise $exercise
     * @param $start
     * @param $limit
     * @return array
     */
    function getActivitiesOfExercise(Exercise $exercise, $start, $limit): array {
        /** @var ActivityRepository $rep */
        $rep = $this->em->getRepository(Activity::class);
        return $rep->getActivitiesOfExercise($exercise, $start, $limit);
    }

    /**
     * Gets all the activities of the given user.
     *
     * @param User $user
     * @param        $limit
     *
     * @return array
     */
    function getActivitiesOfUser(User $user, $limit): array {
        /** @var ActivityRepository $rep */
        $rep = $this->em->getRepository(Activity::class);
        return $rep->getActivitiesOfUser($user, $limit);
    }

    /**
     * Gets an exercise given its id.
     *
     * @param int $id
     *
     * @return Exercise|null
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    function getExercise(int $id): Exercise {
        /** @var ExerciseRepository $rep */
        $rep = $this->em->getRepository(Exercise::class);
        return $rep->getExercise($id);
    }

    /**
     * Gets the amount of activities.
     *
     * @return int
     * @throws NonUniqueResultException
     */
    function getActivitiesCount(): int {
        /** @var ActivityRepository $rep */
        $rep = $this->em->getRepository(Activity::class);
        return $rep->getActivitiesCount();
    }

    /**
     * Gets the amount of exercises.
     *
     * @return int
     * @throws NonUniqueResultException
     */
    function getExercisesCount(): int {
        /** @var ExerciseRepository $rep */
        $rep = $this->em->getRepository(Exercise::class);
        return $rep->getExercisesCount();
    }

    /**
     * Gets the activities count grouped by modes.
     *
     * @return array
     */
    function getActivitiesCountByMode(): array {
        /** @var ActivityRepository $rep */
        $rep = $this->em->getRepository(Activity::class);
        return $rep->getActivitiesCountByMode();
    }
}
