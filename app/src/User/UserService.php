<?php

namespace App\User;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;

class UserService implements UserServiceInterface {

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * Ctor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct ( EntityManagerInterface $em ) {
        $this->em = $em;
    }

    /**
     * Gets the amount of users.
     *
     * @return integer
     *
     * @throws NonUniqueResultException
     */
    function getUsersCount (): int {
        /** @var UserRepository $rep */
        $rep = $this->em->getRepository ( User::class );
        return $rep->getUsersCount ();
    }
}
