<?php

namespace App\User;

interface UserServiceInterface {

    /**
     * Gets the amount of users.
     *
     * @return integer
     */
    function getUsersCount (): int;
}
