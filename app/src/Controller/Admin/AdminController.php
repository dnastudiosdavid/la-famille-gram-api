<?php

namespace App\Controller\Admin;

use App\Exercise\SentencesServiceInterface;
use App\User\UserServiceInterface;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends AbstractController {

    /**
     * Main route.
     * @Route("/admin")
     *
     * @param UserServiceInterface     $userService
     * @param SentencesServiceInterface $exerciseService
     *
     * @return Response
     */
    public function indexAction (UserServiceInterface $userService, SentencesServiceInterface $exerciseService ) {

        return $this->render ( 'admin/stats.html.twig', array (
            'users_count' => $userService->getUsersCount (),
            'exercises_count' => $exerciseService->getExercisesCount (),
            'activities_count' => $exerciseService->getActivitiesCount (),
            'modes' => $exerciseService->getActivitiesCountByMode ()
        ) );
    }
}
