<?php

namespace App\Controller\API\Exercise;

use App\Entity\SubstitutesActivity;
use App\Entity\SubstitutesExercise;
use App\Entity\User;
use App\Exercise\SubstitutesServiceInterface;
use App\Form\Type\SubstitutesActivityType;
use App\Form\Type\SubstitutesExerciseType;
use Doctrine\ORM\NoResultException;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/api/substitutes/")
 */
class SubstitutesController extends AbstractFOSRestController {

    /**
     * Creates a new exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("exercise")
     *
     * @param Request $request
     * @param SubstitutesServiceInterface $exerciseService
     * @param TokenStorageInterface $tokenStorage
     *
     * @return View|FormInterface
     */
    public function createExercise(Request $request, SubstitutesServiceInterface $exerciseService, TokenStorageInterface $tokenStorage) {
        $exercise = new SubstitutesExercise();
        $form = $this->createForm(SubstitutesExerciseType::class, $exercise);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $this->denyAccessUnlessGranted("create", $exercise);
            /** @var User $user */
            $user = $tokenStorage->getToken()->getUser();
            $exercise = $exerciseService->persistExercise($exercise, $user);
            return View::create($exercise);
        }

        return $form;
    }

    /**
     * Updates an exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Put("exercise/{id}")
     *
     * @param Request $request
     * @param SubstitutesExercise $exercise
     * @param SubstitutesServiceInterface $exerciseService
     *
     * @return View|FormInterface
     */
    public function updateExercise(Request $request, SubstitutesExercise $exercise, SubstitutesServiceInterface $exerciseService) {
        $this->denyAccessUnlessGranted("edit", $exercise);
        $form = $this->createForm(SubstitutesExerciseType::class, $exercise);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $exercise = $exerciseService->updateExercise($exercise);
            return View::create($exercise);
        }
        return $form;
    }

    /**
     * Gets all the exercises of the current user.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("exercise")
     *
     * @param TokenStorageInterface $tokenStorage
     * @param SubstitutesServiceInterface $exerciseService
     *
     * @return View
     */
    public function getExercises(TokenStorageInterface $tokenStorage, SubstitutesServiceInterface $exerciseService): View {
        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        return View::create($exerciseService->getExercisesOf($user));
    }

    /**
     * Gets an existing exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("exercise/{id}")
     *
     * @param integer $id
     *
     * @param SubstitutesServiceInterface $exerciseService
     *
     * @return View
     */
    public function getExercise(int $id, SubstitutesServiceInterface $exerciseService): View {
        $exercise = $exerciseService->getExercise($id);
        $this->denyAccessUnlessGranted("view", $exercise);
        return View::create($exercise);
    }

    /**
     * Deletes an existing exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Delete("exercise/{id}")
     *
     * @param SubstitutesExercise $exercise
     * @param SubstitutesServiceInterface $exerciseService
     *
     * @return View
     */
    public function deleteExercise(SubstitutesExercise $exercise, SubstitutesServiceInterface $exerciseService): View {
        $this->denyAccessUnlessGranted("delete", $exercise);
        $exerciseService->deleteExercise($exercise);
        return View::create();
    }

    /**
     * Loads an existing exercise given its code.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("p/exercise/{code}")
     *
     * @param string $code
     * @param SubstitutesServiceInterface $exerciseService
     *
     * @return View
     */
    public function loadExercise(string $code, SubstitutesServiceInterface $exerciseService): View {
        try {
            $exercise = $exerciseService->getExercisesByCode($code);
            return View::create($exercise);
        } catch (NoResultException $e) {
            throw new NotFoundHttpException("Exercise was not found.");
        } catch (Exception $e) {
            throw new BadRequestHttpException("Fatal Error: " . $e->getMessage());
        }
    }

    /**
     * Creates a new activity.
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("p/exercise/{code}/activity")
     *
     * @param Request $request
     * @param string $code
     * @param SubstitutesServiceInterface $exerciseService
     *
     * @return View|FormInterface
     */
    public function createActivity(Request $request, string $code, SubstitutesServiceInterface $exerciseService) {
        $activity = new SubstitutesActivity();
        $form = $this->createForm(SubstitutesActivityType::class, $activity);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            try {
                $exerciseService->persistActivity($activity, $code);
            } catch (Exception $e) {
                if ($e instanceof NoResultException) {
                    throw new NotFoundHttpException("Exercise was not found.");
                } else {
                    throw new BadRequestHttpException("A fatal error occurred: " . $e->getMessage());
                }
            }
            return View::create();
        }
        return $form;
    }

    /**
     * Gets all the activities of the current user.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("exercise/{id}/activity")
     *
     * @QueryParam(name="start", requirements="\d+", default="0", description="Début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="Nombre d'éléments max par requête")
     *
     * @param SubstitutesExercise $exercise
     * @param SubstitutesServiceInterface $exerciseService
     * @param ParamFetcher $paramFetcher
     *
     * @return View
     */
    public function getActivities(SubstitutesExercise $exercise, SubstitutesServiceInterface $exerciseService, ParamFetcher $paramFetcher): View {
        $this->denyAccessUnlessGranted("view", $exercise);
        $start = $paramFetcher->get("start");
        $limit = $paramFetcher->get("limit");
        return View::create($exerciseService->getActivitiesOfExercise($exercise, $start, $limit));
    }

    /**
     * Gets all the activities of the current user.
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"headers"})
     * @Rest\Get("activity")
     *
     * @QueryParam(name="limit", requirements="\d+", default="20", description="Nombre d'éléments max par requête")
     *
     * @param SubstitutesServiceInterface $exerciseService
     * @param ParamFetcher $paramFetcher
     * @param TokenStorageInterface $tokenStorage
     *
     * @return View
     */
    public function getLastActivitiesOf(SubstitutesServiceInterface $exerciseService, ParamFetcher $paramFetcher, TokenStorageInterface $tokenStorage): View {
        $limit = $paramFetcher->get("limit");
        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        return View::create($exerciseService->getActivitiesOfUser($user, $limit));
    }

    /**
     * Gets an activity.
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"details"})
     * @Rest\Get("activity/{id}")
     *
     * @param SubstitutesActivity $activity
     *
     * @return View
     */
    public function getActivity(SubstitutesActivity $activity): View {
        $this->denyAccessUnlessGranted("view", $activity);
        return View::create($activity);
    }
}
