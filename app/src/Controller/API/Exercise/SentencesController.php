<?php

namespace App\Controller\API\Exercise;

use App\Entity\Activity;
use App\Entity\Exercise;
use App\Entity\Score;
use App\Entity\User;
use App\Exercise\SentencesServiceInterface;
use App\Form\Type\ActivityType;
use App\Form\Type\ExerciseType;
use App\Form\Type\ScoreType;
use Doctrine\ORM\NoResultException;
use Exception;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * TODO: should be /api/sentences/ now - will need a refactor on both clients
 * @Route("/api/")
 */
class SentencesController extends AbstractFOSRestController {

    /**
     * Creates a new exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("exercise")
     *
     * @param Request $request
     * @param SentencesServiceInterface $exerciseService
     * @param TokenStorageInterface $tokenStorage
     *
     * @return View|FormInterface
     */
    public function createExercise(Request $request, SentencesServiceInterface $exerciseService, TokenStorageInterface $tokenStorage) {
        $exercise = new Exercise();
        $form = $this->createForm(ExerciseType::class, $exercise);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $this->denyAccessUnlessGranted("create", $exercise);
            /** @var User $user */
            $user = $tokenStorage->getToken()->getUser();
            $exercise = $exerciseService->persistExercise($exercise, $user);
            return View::create($exercise);
        }

        return $form;
    }

    /**
     * Updates an exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Put("exercise/{id}")
     *
     * @param Request $request
     * @param Exercise $exercise
     * @param SentencesServiceInterface $exerciseService
     *
     * @return View|FormInterface
     */
    public function updateExercise(Request $request, Exercise $exercise, SentencesServiceInterface $exerciseService) {
        $this->denyAccessUnlessGranted("edit", $exercise);
        $form = $this->createForm(ExerciseType::class, $exercise);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $exercise = $exerciseService->updateExercise($exercise);
            $this->fixExerciseModes($exercise);
            return View::create($exercise);
        }
        return $form;
    }

    /**
     * Gets all the exercises of the current user.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("exercise")
     *
     * @param TokenStorageInterface $tokenStorage
     * @param SentencesServiceInterface $exerciseService
     *
     * @return View
     */
    public function getExercises(TokenStorageInterface $tokenStorage, SentencesServiceInterface $exerciseService): View {
        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        return View::create($exerciseService->getExercisesOf($user));
    }

    /**
     * Gets an existing exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("exercise/{id}")
     *
     * @param integer $id
     *
     * @param SentencesServiceInterface $exerciseService
     *
     * @return View
     */
    public function getExercise(int $id, SentencesServiceInterface $exerciseService): View {
        $exercise = $exerciseService->getExercise($id);
        $this->denyAccessUnlessGranted("view", $exercise);
        $this->fixExerciseModes($exercise);
        return View::create($exercise);
    }

    /**
     * Deletes an existing exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Delete("exercise/{id}")
     *
     * @param Exercise $exercise
     * @param SentencesServiceInterface $exerciseService
     *
     * @return View
     */
    public function deleteExercise(Exercise $exercise, SentencesServiceInterface $exerciseService): View {
        $this->denyAccessUnlessGranted("delete", $exercise);
        $exerciseService->deleteExercise($exercise);
        return View::create();
    }

    /**
     * Loads an existing exercise to be played.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("p/exercise/{code}")
     *
     * @param string $code
     * @param SentencesServiceInterface $exerciseService
     *
     * @return View
     */
    public function loadExercise(string $code, SentencesServiceInterface $exerciseService): View {
        try {
            $exercise = $exerciseService->getExercisesByCode($code);
            $this->fixExerciseModes($exercise);
            return View::create($exercise);
        } catch (Exception $e) {
            if ($e instanceof NoResultException) {
                throw new NotFoundHttpException("Exercise was not found.");
            } else {
                throw new BadRequestHttpException("A fatal error occurred: " . $e->getMessage());
            }
        }
    }

    /**
     * Creates a new score entry.
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("p/exercise/{code}/score")
     *
     * @param Request $request
     * @param string $code
     * @param SentencesServiceInterface $exerciseService
     *
     * @return View|FormInterface
     */
    public function createScore(Request $request, string $code, SentencesServiceInterface $exerciseService): View {
        $score = new Score();
        $form = $this->createForm(ScoreType::class, $score);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            try {
                $exerciseService->persistScore($score, $code);
            } catch (Exception $e) {
                if ($e instanceof NoResultException) {
                    throw new NotFoundHttpException("Exercise was not found.");
                } else {
                    throw new BadRequestHttpException("A fatal error occurred: " . $e->getMessage());
                }
            }
            return View::create();
        }
        return $form;
    }

    /**
     * Updates a score entry.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Put("exercise/{exercise}/score/{score}")
     *
     * @param Request $request
     * @param Exercise $exercise
     * @param Score $score
     * @param SentencesServiceInterface $exerciseService
     *
     * @return View|FormInterface
     */
    public function updateScore(Request $request, Exercise $exercise, Score $score, SentencesServiceInterface $exerciseService): View {
        $this->denyAccessUnlessGranted("edit", $exercise);
        $form = $this->createForm(ScoreType::class, $score);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $score = $exerciseService->updateScore($score);
            $leaderboard = $exerciseService->getLeaderboardOf($exercise->getCode(), $score->getMode());
            return View::create($leaderboard);
        }
        return $form;
    }

    /**
     * Gets the leaderboard of a given exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("p/exercise/{code}/score")
     *
     * @QueryParam(name="mode", allowBlank=false, description="Game mode")
     *
     * @param string $code
     * @param SentencesServiceInterface $exerciseService
     * @param ParamFetcher $paramFetcher
     *
     * @return View
     */
    public function getLeaderboard(string $code, SentencesServiceInterface $exerciseService, ParamFetcher $paramFetcher): View {
        $leaderboard = $exerciseService->getLeaderboardOf($code, $paramFetcher->get("mode"));
        return View::create($leaderboard);
    }

    /**
     * Creates a new activity.
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("p/exercise/{code}/activity")
     *
     * @param Request $request
     * @param string $code
     * @param SentencesServiceInterface $exerciseService
     *
     * @return View|FormInterface
     */
    public function createActivity(Request $request, string $code, SentencesServiceInterface $exerciseService) {
        $activity = new Activity();
        $form = $this->createForm(ActivityType::class, $activity);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            try {
                $exerciseService->persistActivity($activity, $code);
            } catch (Exception $e) {
                if ($e instanceof NoResultException) {
                    throw new NotFoundHttpException("Exercise was not found.");
                } else {
                    throw new BadRequestHttpException("A fatal error occurred: " . $e->getMessage());
                }
            }
            return View::create();
        }
        return $form;
    }

    /**
     * Gets an existing exercise.
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"details"})
     * @Rest\Get("activity/{id}")
     *
     * @param Activity $activity
     *
     * @return View
     */
    public function getActivity(Activity $activity): View {
        $this->denyAccessUnlessGranted("view", $activity);
        return View::create($activity);
    }

    /**
     * Gets all the activities of the current user.
     *
     * @Rest\View(statusCode=Response::HTTP_OK)
     * @Rest\Get("exercise/{id}/activity")
     *
     * @QueryParam(name="start", requirements="\d+", default="0", description="Début de la pagination")
     * @QueryParam(name="limit", requirements="\d+", default="20", description="Nombre d'éléments max par requête")
     *
     * @param Exercise $exercise
     * @param SentencesServiceInterface $exerciseService
     * @param ParamFetcher $paramFetcher
     *
     * @return View
     */
    public function getActivitiesOf(Exercise $exercise, SentencesServiceInterface $exerciseService, ParamFetcher $paramFetcher): View {
        $this->denyAccessUnlessGranted("view", $exercise);
        $start = $paramFetcher->get("start");
        $limit = $paramFetcher->get("limit");
        return View::create($exerciseService->getActivitiesOfExercise($exercise, $start, $limit));
    }

    /**
     * Gets all activities (with filters).
     *
     * @Rest\View(statusCode=Response::HTTP_OK, serializerGroups={"headers"})
     * @Rest\Get("activity")
     *
     * @param SentencesServiceInterface $exerciseService
     * @param ParamFetcher $paramFetcher
     * @param TokenStorageInterface $tokenStorage
     *
     * @return View
     *
     * @QueryParam(name="limit", requirements="\d+", default="15", description="Nombre d'éléments à récupérer.")
     */
    public function getActivities(SentencesServiceInterface $exerciseService, ParamFetcher $paramFetcher, TokenStorageInterface $tokenStorage): View {
        /** @var integer $sort */
        $limit = $paramFetcher->get("limit");
        /** @var User $user */
        $user = $tokenStorage->getToken()->getUser();
        return View::create($exerciseService->getActivitiesOfUser($user, $limit));
    }

    /**
     * Quick & dirty fix.
     * https://github.com/schmittjoh/JMSSerializerBundle/issues/373
     * @param Exercise|null $exercise
     */
    private function fixExerciseModes(?Exercise $exercise) {
        $modes = [];
        foreach ($exercise->getModes() as $i => $mode) {
            $modes[] = $mode;
        }
        $exercise->setModes($modes);
    }
}
