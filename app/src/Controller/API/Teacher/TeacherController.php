<?php

namespace App\Controller\API\Teacher;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @Route("/api/")
 */
class TeacherController extends AbstractFOSRestController {

    private $eventDispatcher;
    private $formFactory;
    private $userManager;
    private $tokenStorage;

    /**
     * TeacherController constructor.
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param FactoryInterface $formFactory
     * @param UserManagerInterface $userManager
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, FactoryInterface $formFactory, UserManagerInterface $userManager, TokenStorageInterface $tokenStorage) {
        $this->eventDispatcher = $eventDispatcher;
        $this->formFactory = $formFactory;
        $this->userManager = $userManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Creates a new teacher.
     *
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("p/teacher")
     *
     * @param Request $request
     *
     * @return UserInterface|Response
     */
    public function postTeacherAction(Request $request) {

        $user = $this->userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $this->eventDispatcher->dispatch($event, FOSUserEvents::REGISTRATION_INITIALIZE);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->formFactory->createForm(array("csrf_protection" => false));
        $form->setData($user);
        $form->submit($request->request->all());

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch($event, FOSUserEvents::REGISTRATION_SUCCESS);

            $this->userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                return $response;
            }

            // Don't send the registration completed event, because
            // it will log the user in.
            // https://stackoverflow.com/questions/17038775/registering-user-with-fosuserbundle-without-logging-the-user-in
            // $this->eventDispatcher->dispatch ( FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent( $user, $request, $response ) );

            return $response;
        } else {
            $event = new FormEvent($form, $request);
            $this->eventDispatcher->dispatch($event, FOSUserEvents::REGISTRATION_FAILURE);

            if (null !== $response = $event->getResponse()) {
                return $response;
            }

            return $form;
        }
    }
}
