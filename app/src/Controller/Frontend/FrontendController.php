<?php

namespace App\Controller\Frontend;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontendController {

    /**
     * @Route("/")
     *
     * @return Response
     *
     */
    public function number () {
        return new Response(
            '<html><body><h1>Hello everybody!</h1></body></html>'
        );
    }
}
